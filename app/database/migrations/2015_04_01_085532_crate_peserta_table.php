<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CratePesertaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('peserta', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nama_lengkap');
			$table->string('asal_sekolah');
			$table->string('nomor_un');
			$table->string('email');
			$table->string('password');
			$table->string('bukti');
			$table->integer('status')->default(0);
			$table->string('verification_code');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
