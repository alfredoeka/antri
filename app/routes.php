<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('layout.main');
});


Route::get('/tes', function()
{
	return "hola";
});

Route::group(array('before' => 'csrf'), function() 
{
	Route::post('/tes', array(
		'as' => 'post-daftar',
		'uses' => 'AccountController@postDaftar'
	));

	
});