<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
   		<meta http-equiv="X-UA-Compatible" content="IE=edge">
   		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		{{ HTML::style('css/bootstrap.min.css') }}
		{{ HTML::style('css/main.css') }}
		
		<title>
			@if(Auth::check() == false)
				SMENDA PPDB Goes Online
			@endif
		</title>
	</head>
	<body>
	<nav class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
		    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		    	  <span class="sr-only">Toggle navigation</span>
		    	  <span class="icon-bar"></span>
		    	  <span class="icon-bar"></span>
		    	  <span class="icon-bar"></span>
		    	</button>
		    	<a class="navbar-brand" href="#"><span class="glyphicon glyphicon-home"></span>&ensp;Antre</a>
		  	</div>
		</div>
	</nav>
	<div class="container">
		<div class="col-md-4"></div>
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<h3>Buat Akun</h3>
			<hr>
			
			{{ Form::open(array('route' => 'post-daftar', 'role' => 'form', 'files' => TRUE)) }}
			
				<div class="form-group">
					
					{{ Form::label('namalengkap', 'Nama Lengkap') }}
					{{ Form::text('namalengkap', null, array('class' => 'form-control')) }}

				</div>
				<div class="form-group">

					{{ Form::label('asalsekolah', 'Asal Sekolah') }}
			
					<select id="asalsekolah" class="form-control">
						<option>SMP Negeri 1 Candi</option>
						<option>SMP Negeri 2 Candi</option>
					</select>

				</div>
				<div class="form-group">
					
					{{ Form::label('nomorun', 'Nomor Peserta UN') }}
					{{ Form::text('nomorun', null, array('class' => 'form-control')) }}	
				
				</div>
				<div class="form-group">

					{{ Form::label('email', 'E-mail') }}
					{{ Form::email('email', null, array('class' => 'form-control')) }}
					
				</div>
				<div class="form-group">

					{{ Form::label('password', 'Password') }}
					<input type="password" class="form-control" id="password">

				</div>

			{{ Form::submit('Buat') }}
			{{ Form::close() }}
			
		</div>	
	</div>
</html>